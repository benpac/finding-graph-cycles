#include "intersection_helper.h"

IntersectionHelper::Orientation IntersectionHelper::getOrientation(const Point& p1, const Point& p2, const Point& p3) {
  tmp =
    (p2.getY() - p1.getY()) * (p3.getX() - p2.getX()) - 
    (p3.getY() - p2.getY()) * (p2.getX() - p1.getX());
  if (IS_ZERO(tmp)) { return OrientNone; }
  return (tmp < 0) ? OrientCounterClockwise : OrientClockwise;
}

bool IntersectionHelper::intersects(const Segment& s1, const Segment& s2) {
  Point *p0 = s1.getP0();
  Point *p1 = s1.getP1();
  Point *q0 = s2.getP0();
  Point *q1 = s2.getP1();

  o1 = getOrientation(*p0,*p1,*q0);
  o2 = getOrientation(*p0,*p1,*q1);
  o3 = getOrientation(*q0,*q1,*p0);
  o4 = getOrientation(*q0,*q1,*p1);
  
  // General case
  if (o1 != o2 && o3 != o4)
    { return true; }
  
  // Special Cases
  // p0, p1 and q0 are colinear and q0 lies on segment p0p1
  if (o1 == OrientNone && onSegment(*p0, *q0, *p1)) return true;
 
  // p0, p1 and q0 are colinear and q1 lies on segment p0p1
  if (o2 == OrientNone && onSegment(*p0, *q1, *p1)) return true;
 
  // q0, q1 and p0 are colinear and p0 lies on segment q0q1
  if (o3 == OrientNone && onSegment(*q0, *p0, *q1)) return true;
 
  // q0, q1 and p1 are colinear and p1 lies on segment q0q1
  if (o4 == OrientNone && onSegment(*q0, *p1, *q1)) return true;

  return false;
}

bool IntersectionHelper::onSegment(const Point& p1, const Point& p2, const Point& p3) {
    if (p2.getX() <= std::max(p1.getX(), p3.getX()) && p2.getX() >= std::min(p1.getX(), p3.getX()) &&
        p2.getY() <= std::max(p1.getY(), p3.getY()) && p2.getY() >= std::min(p1.getY(), p3.getY()))
      { return true; }
 
    return false;
}
