#include "CImg_include/CImg.h"
#include "undirected_graph.h"
#include "point.h"
#include "graph_physics.h"
#include "graph_reader.h"

#include <vector>

using namespace cil;

const unsigned char
white[]  = { 255, 255, 255 }, black[] = { 0, 0, 0 };

bool isAllOnScreen(double screen_x, double screen_y, std::vector<Point>& points) {
  for (unsigned int p = 0; p < points.size(); p++) {
    if(points[p].getX() < 0 || points[p].getX() > screen_x ) {
      return false;
    }
    if(points[p].getY() < 0 || points[p].getY() > screen_y ) {
      return false;
    }
  }
  return true;
}

void recenter(double s_x, double s_y, GraphPhysics& gp) {
  Point center_graph = gp.computeCenterBox();
  Point translation(s_x/2 - center_graph.getX(), s_y/2 - center_graph.getY());
  gp.translateAll(translation);
}

void colorCycles(CImg<unsigned char>& visu,
                 std::vector< Point >& points,
                 CImgDisplay& CI_display, 
                 UndirectedGraph &g,
                 GraphPhysics &gp) {
  std::vector<std::vector<int> > cycles = g.findAllCycles();
  for (unsigned int c = 0; c < cycles.size(); c++) {
    //color
    unsigned char r,g,b;
    r = (unsigned char)(gp.rand(255));    g =(unsigned char)(gp.rand(255));    b = (unsigned char)(gp.rand(255));
    const unsigned char color[] = {r, g, b};
    //prepare points
    CImg<int> poly_points(cycles[c].size(),2);
    for (unsigned int p = 0; p < cycles[c].size(); p++) {
      poly_points(p,0) = points[cycles[c][p]].getX();
      poly_points(p,1) = points[cycles[c][p]].getY();
    }
    visu.draw_polygon(poly_points, color);
    //delete color;
  }

  CI_display.display(visu);
}

void printPoints(CImg<unsigned char>& visu, 
                 std::vector< Point >& points,
                 std::vector<int>& conn,
                 CImgDisplay& CI_display, 
                 double dezoom_factor) {
  visu.fill(255);
  int nb_nodes = points.size();
  int nb_edges = conn.size() / 2;

  for ( int i = 0; i < nb_nodes; i++) {
    visu.draw_circle(points[i].getX(),
                     points[i].getY(),
                     5 * dezoom_factor,
                     black);
  }
  for ( int i = 0; i < nb_edges; i++) {
    visu.draw_line(points[conn[2*i]].getX(),
                   points[conn[2*i]].getY(),
                   points[conn[2*i+1]].getX(),
                   points[conn[2*i+1]].getY(),black);
  }
  //  visu.display(CI_display);
  CI_display.display(visu);
  //CI_display.render();
}


int main(int argc, char *argv[])
{
  std::string file_name = "";
  if (argc >= 2) {
    file_name = std::string(argv[1]);
  }
  if (file_name.empty()) { std::cout << "Please enter file name" << std::endl; return 0;}
  double factor(0.01), obj_distance(100), tol(-1);
  if (argc >= 5) {
    factor = atof(argv[2]);
    obj_distance = atof(argv[3]);
    tol = atof(argv[4]);
  }
  bool adaptive_tol = false;
  if (tol < 0) { adaptive_tol = true; }
  double size_x = 500;
  double size_y = 400;
  if (argc >= 7) {
    size_x = atof(argv[5]);
    size_y = atof(argv[6]);
  }
  int seed = time(0);
  bool verbose = false;
  if (argc >= 8) {
    int arg_verbose = atoi(argv[7]);
    if (arg_verbose == 0) { verbose = false; }
  }
  if (argc >= 9) {
    seed = atoi(argv[8]);
  }
  if (seed==0) { seed = (int)(time(0)); }
  std::srand(seed);

  
  double dezoom_factor = 1.0;
  //create the graph from file
  GraphReader gr(file_name);
  UndirectedGraph graph = gr.read();
  
  graph.findAllCycles();

  GraphPhysics gp(graph);
  
  int nb_nodes = graph.getV();
  CImg<unsigned char> visu(size_x,size_y,1,3,0);

  std::vector< Point > points = gp.getPoints();
  for ( int i = 0; i < nb_nodes; i++) {
    points[i].setX(cimg::rand(0,size_x));
    points[i].setY(cimg::rand(0,size_y));
  }
  gp.setPoints(points);

  std::vector<int> conn = gp.getConn();

  CImgDisplay graph_disp(visu, "graph", 0);
  printPoints(visu,points, conn,graph_disp, dezoom_factor);
  if (adaptive_tol) { tol = gp.countIntersections() > 10 ? 10*gp.countIntersections() : 500; }
  unsigned int iter(0), swaps(0), stuck(0);
  bool converged = false;
  cimg::tic();
  while (!graph_disp.is_closed() ) {
    iter++;
    gp.movePoints(obj_distance, obj_distance, factor);
    printPoints(visu, gp.getPoints(), conn, graph_disp, dezoom_factor);
    converged = gp.checkForceNorm(tol);
    if (converged) {
      if (!isAllOnScreen(size_x,size_y,gp.getPoints())) {
        if (verbose) { std::cout << "recentering... ";}
        recenter(size_x,size_y, gp);
        if (verbose) { std::cout << "dezooming..." << std::endl;}
        obj_distance *= 0.9;
        dezoom_factor *= 0.925;
      }
      int nb_inter = gp.countIntersections();
      if (adaptive_tol) { tol = nb_inter > 100 ? 2*nb_inter : (nb_inter > 10 ? 5*nb_inter : 200); }
      if (verbose) { std::cout << "nb intersections : " << nb_inter << ". "; }
      if (nb_inter > 0) {
        if (verbose) { std::cout << "swapping edges...";}
        bool success = gp.switchPoints();
        swaps++;
        if (success) { if (verbose) { std::cout << " success!" << std::endl; }}
        else { 
          if (verbose) { std::cout << " unsuccessfull... :(" << std::endl;}
          int ind1 = gp.rand(graph.getV());
          int ind2 = gp.rand(graph.getV());
          if (verbose) { std::cout << "swapping : " << ind1 << "," << ind2 << std::endl;}
          stuck++;
          gp.swap(ind1,ind2);
        }
        printPoints(visu, gp.getPoints(), conn, graph_disp, dezoom_factor);
      } else {
        if (isAllOnScreen(size_x,size_y,gp.getPoints())) {
          colorCycles(visu, gp.getPoints(), graph_disp, graph, gp);
            std::cout << "Done in " << iter << " iterations and " << swaps 
                      << " swaps! Getting stuck " <<stuck << " times." << std::endl;
          cimg::toc();
          // unsigned long int time_passed = cimg::toc();
          // std::cout << "All that in : " << time_passed / 1000 << " seconds (" 
          //           <<  time_passed / 60000 << " minuts)" << std::endl;
          cimg::wait(2000);
          if (verbose) { std::cout << "final obj_distance : " << obj_distance << std::endl;}
          visu.save((file_name + std::string(".jpg")).c_str());
          return 0;
        } else {
          if (verbose) { std::cout << "recentering... ";}
          recenter(size_x,size_y, gp);
          if (verbose) { std::cout << "dezooming..." << std::endl;}
          obj_distance *= 0.9;
          dezoom_factor *= 0.925;
        }
      }
      converged = false;
    }
  }

  return 0;
}
