/** 
 * @file intersection_helper.h
 *
 * @author Benjamin Paccaud <bpaccaud@gmail.com>
 * 
 * @date 27-Jan-2016
 *
 * @brief 
 */


#ifndef __INTERSECTION_HELPER_H__
#define __INTERSECTION_HELPER_H__

#include "point.h"
#include "segment.h"
#include <cmath>

#define IS_ZERO(value) (std::abs(value) < 1e-12)

class IntersectionHelper
{
public:
  enum Orientation  {
    OrientNone,
    OrientClockwise,
    OrientCounterClockwise
  };
  
  IntersectionHelper() {};
  virtual ~IntersectionHelper() {};
  
  //Acessors
public:
  
  
  //Methods
  bool intersects(const Segment& s1, const Segment& s2);

  Orientation getOrientation(const Point& p1, const Point& p2, const Point& p3);
  bool onSegment(const Point& p1, const Point& p2, const Point& p3);
  //Variables
private:
  int o1;
  int o2;
  int o3;
  int o4;
  
  double tmp;
};

#endif /* __INTERSECTION_HELPER_H__ */
