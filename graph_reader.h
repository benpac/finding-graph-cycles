/** 
 * @file graph_reader.h
 *
 * @author Benjamin Paccaud <bpaccaud@gmail.com>
 * 
 * @date 28-Jan-2016
 *
 * @brief 
 */


#ifndef __GRAPH_READER_H__
#define __GRAPH_READER_H__

#include "undirected_graph.h"
#include <string>

///Class to read graph information (number of nodes and edges) from a file.
class GraphReader
{
public:
  ///constructor
  GraphReader() : file_name(std::string("")) {};
  GraphReader(std::string _file) : file_name(_file) {};
  GraphReader(const GraphReader& g) : file_name(g.getFileName()) {};
  
  virtual ~GraphReader() {};
  
  //Acessors
public:
  ///Sets the graph file name
  void setFileName(std::string _file) { file_name = _file; };
  ///getter for the file_name
  std::string getFileName() const { return file_name; };
  //Methods
  
  ///Reads file_name and constructs the graph from the informations.
  ///\returns An UndirectedGraph constructed from informations in the file. If the file is not valid, will return an empty graph.
  UndirectedGraph read();
  ///Sets file_name to file and calls read()
  UndirectedGraph read(std::string file);
  ///splits the string \a s using \a delim and puts the results into elems
  ///
  ///taken from <a"http://stackoverflow.com/a/236803/4983562>this stackoverflow post</a>
  static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
  ///splits the string \a s using \a delim 
  ///\returns a newly created vector containing the splitted string
  ///
  ///taken from <a"http://stackoverflow.com/a/236803/4983562>this stackoverflow post</a>
  static std::vector<std::string> split(const std::string &s, char delim);
  
  ///\returns the int contained in \a str. If \a str is not valid, will returns 0
  int getInt(std::string str);
  ///\returns the double contained in \a str. If \a str is not valid, will returns 0
  double getDouble(std::string str);

  //Variables
private:
  ///path to file to read the graph from
  std::string file_name;
};

#endif /* __GRAPH_READER_H__ */
