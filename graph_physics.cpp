#include "graph_physics.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

GraphPhysics::GraphPhysics(UndirectedGraph _g) :
  g(_g),
  points(_g.getV()),
  conn(_g.getEdges()),
  distances(_g.getE()),
  vectors(_g.getE()),
  forces(_g.getV()),
  init_rand(true)
{
  //yeah
  createSegments();
}

void GraphPhysics::setGraph(UndirectedGraph new_g) {
  g = new_g;
  conn = g.getEdges();
  distances = std::vector<double>(g.getE());
  vectors = VecPoint(g.getE());
  forces = VecPoint(g.getV());
}

void GraphPhysics::setPoints(std::vector<Point> new_p) {
  if (new_p.size() != points.size()) {
    std::cout << "Error: setting points impossible, new point not same size !" << std::endl;
    return;
  }
  for (unsigned int i = 0; i < points.size(); i++) {
    points[i].setX(new_p[i].getX());
    points[i].setY(new_p[i].getY());
  }
}


void GraphPhysics::translateAll(Point p) {
  for (unsigned int i = 0; i < points.size(); i++) {
    points[i] += p;
  }
}


void GraphPhysics::computeVectors(){
  for (unsigned int e = 0; e < conn.size()/2; e++) {
    vectors[e] = points[conn[2*e]].getVector( points[conn[2*e+1]] );
  }
}

void GraphPhysics::computeDistances(){
  //std::cout << "conn size " << conn.size() << " /2 = " << conn.size() /2 << std::endl;
  for (unsigned int e = 0; e < conn.size()/2; e++) {
    //std::cout << points[conn[2*e]] << points[conn[2*e+1]] << std::endl;
    distances[e] = points[conn[2*e]].distance(points[conn[2*e+1]]);
  }
}
  
void GraphPhysics::computeForces(double obj_distance){
  for (unsigned int e = 0; e < vectors.size(); e++) {
    forces[conn[2*e  ]] +=  vectors[e]*ratio(obj_distance,distances[e]);
    forces[conn[2*e+1]] += -vectors[e]*ratio(obj_distance,distances[e]);
  }
}

void GraphPhysics::computeRepulsion(double rep_range){
 VecPoint ret(points.size());
  for (unsigned int p = 0; p < points.size(); p++) {
    for (unsigned int pp = 0; pp < points.size(); pp++) {
      if (p == pp) { continue; }
      //      std::cout << " doing : " << p << " " << pp << std::endl;
      double d = points[p].distance(points[pp]);
      forces[p] += -points[p].getVector(points[pp]) * (50 * shift(d, rep_range));
    }
  }
}
  
void GraphPhysics::zerosVecPoint(VecPoint& v){
  for (unsigned int i = 0; i < v.size(); i++) {
    v[i].zero();
  }
}

///Assumes val and obj are positive
double GraphPhysics::shift(double val, double obj) {
  if (val > obj) { return 0; }
  else {
    double d = val / obj;
    return (1 - d*d) * (1 - d*d) * (1 - d*d) * (1 - d*d);
  }
}

void GraphPhysics::applyForces(double factor) {
  for (unsigned int p = 0; p < points.size(); p++) {
    points[p] += forces[p] * factor;
  }
}

void GraphPhysics::movePoints(double obj, double rep, double factor, bool do_rep) {
  //  std::cout << "zeroing...";
  zerosVecPoint(forces);
  //  std::cout << "comp. vec...";
  computeVectors();
  //  std::cout << "comp. dist...";
  computeDistances();
  //  std::cout << "comp. forces...";
  computeForces(obj);
  //  std::cout << "comp. rep...";
  if (do_rep) {
    computeRepulsion(rep);
  }
  //  std::cout << "apply forces...";
  applyForces(factor);
  //  std::cout << "done" << std::endl;
}
  
bool GraphPhysics::checkForceNorm(double tol) {
  return norm(forces) < tol;
}

double GraphPhysics::norm(VecPoint& v) {
  double norm(0.);
  for (unsigned int i = 0; i < v.size(); i++) {
    norm += v[i].norm();
  }
  return norm;
}

Point GraphPhysics::computeCenter() {
  double x(0.), y(0.);
  int size = points.size();
  for ( int i = 0; i < size; i++) {
    x += points[i].getX();
    y += points[i].getY();
  }
  return Point(x/size, y/size);
}

Point GraphPhysics::computeCenterBox() {
  double x_min(1e12), y_min(1e12);
  double x_max(-1e12), y_max(-1e12);
  int size = points.size();
  double tmp_x, tmp_y;
  for ( int i = 0; i < size; i++) {
    tmp_x = points[i].getX();
    tmp_y = points[i].getY();
    if (tmp_x < x_min) { x_min = tmp_x; }
    if (tmp_y < y_min) { y_min = tmp_y; }
    if (tmp_x > x_max) { x_max = tmp_x; }
    if (tmp_y > y_max) { y_max = tmp_y; }
  }  
  return Point(x_min + (x_max-x_min) / 2, y_min + (y_max-y_min) / 2);
}

void GraphPhysics::createSegments() {
  segments.clear();
  for (unsigned int e = 0; e < g.getE(); e++) {
    segments.push_back(Segment(&points[conn[2*e]],
                               &points[conn[2*e+1]]));
  }
}

int GraphPhysics::countIntersections() {
  int ret(0);
  // std::cout << "size segments : " << segments.size() << std::endl;
  for (unsigned int s = 0; s < segments.size(); s++) {
    for (unsigned int inner_s = s+1; inner_s < segments.size(); inner_s++) {
      if ( ( (segments[s].getP0() != segments[inner_s].getP0()) && 
             (segments[s].getP0() != segments[inner_s].getP1()) ) &&
           ( (segments[s].getP1() != segments[inner_s].getP0()) && 
             (segments[s].getP1() != segments[inner_s].getP1()) )) {
        if (inter_helper.intersects(segments[s],segments[inner_s])) {
          // std::cout << "s        : " << *(segments[s].getP0()) << ":" << *(segments[s].getP1()) << std::endl;
          // std::cout << "inner_ss : " << *(segments[inner_s].getP0()) << ":" << *(segments[inner_s].getP1()) << std::endl;
          ++ret;
        }
      }
    }
  }
  return ret;
}

bool GraphPhysics::switchPoints(bool strict) {
  int nb_cross = countIntersections();
  int try_ind = rand(nb_cross);
  //  std::cout << "rand start : " << try_ind <<  std::endl;
  int ind(0);
  std::vector<std::pair<Segment*, Segment*> > seg_pairs;
  //find all relevent crossing segments
  for (unsigned int s = 0; s < segments.size(); s++) {
    for (unsigned int inner_s = s+1; inner_s < segments.size(); inner_s++) {
      if ( ( (segments[s].getP0() != segments[inner_s].getP0()) && 
             (segments[s].getP0() != segments[inner_s].getP1()) ) &&
           ( (segments[s].getP1() != segments[inner_s].getP0()) && 
             (segments[s].getP1() != segments[inner_s].getP1()) )) {
        if (inter_helper.intersects(segments[s],segments[inner_s])) {
          //the segment do not have a point in common and they intersect
          seg_pairs.push_back(std::pair<Segment*, Segment*>(&segments[s], &segments[inner_s]));
          ind++;
        }
      }
    }
  }
  //loop through the crossing segments starting from a random one (avoid always switching the same)
  for (unsigned int p = 0; p < seg_pairs.size(); p++) {
    std::pair<Segment*, Segment*> cross = seg_pairs[try_ind];
    //try swapping two point and count the nb of intersections.
    std::vector<std::pair<Point*, Point*> > points_pairs;
    points_pairs.push_back(std::pair<Point*, Point*>(cross.first->getP0(), cross.second->getP0()));
    points_pairs.push_back(std::pair<Point*, Point*>(cross.first->getP0(), cross.second->getP1()));
    points_pairs.push_back(std::pair<Point*, Point*>(cross.first->getP1(), cross.second->getP0()));
    points_pairs.push_back(std::pair<Point*, Point*>(cross.first->getP1(), cross.second->getP1()));
    int p_rand = rand(points_pairs.size()-1);
    //loop throug the pair of points we caqn swap
    //staring from a random one to avoid always switching the same ones
    for (unsigned int inside_p = 0; inside_p < points_pairs.size(); inside_p++) {
      std::pair<Point*, Point*> to_cross = points_pairs[p_rand];
      if (trySwapping(nb_cross, to_cross.first, to_cross.second,  strict)) {
        return true;
      }
      ++p_rand; 
      if (p_rand == (int)(points_pairs.size())) { p_rand = 0; }
    }
    ++try_ind;
    if (try_ind == (int)(seg_pairs.size())) { try_ind = 0; }
  }
  //if we are here, we were unseccessful. Try without strict (only if strict, so we dont get infinite loops)
  if (strict) {
    //    std::cout << " still trying ...";
    return switchPoints(false);
  }
  return false;
}

bool GraphPhysics::trySwapping(int nb_cross, Point* p1, Point* p2, bool strict) {
  p1->swap(*(p2));
  int new_nb_cross = countIntersections();
  if (smaller(new_nb_cross,nb_cross,strict)) {
    return true;
  } else { //revert the points
    p1->swap(*(p2));
  }
  return false;
}

bool GraphPhysics::smaller(double val1, double val2, bool strict) {
  if (strict) {
    return val1 < val2;
  }
  else {
    return val1 <= val2;
  }
}

int GraphPhysics::rand(int max) {
  int floor = 0, ceiling = max, range = (ceiling - floor);
  //  std::cout << "f,c,r : " << floor << ", " << ceiling << ", " << range << ", " << std::endl;
  long int r = std::rand();
  //  std::cout << "std::rand() -> " << r << std::endl;
  //  std::cout << "range * r / rand_max " << range*r << " / " << RAND_MAX << std::endl;
  long int tmp = (long int)((range * r) / (RAND_MAX + 1.0));
  //  std::cout << "tmp : " << tmp << std::endl;
  int rnd = floor + tmp;
  //  std::cout << "rnd : " << rnd << std::endl;
  return rnd;
}

void GraphPhysics::swap(int p1, int p2) {
  //std::cout << points[p1] << "," << points[p2] << std::endl;
  double tmpx, tmpy;
  tmpx = points[p1].getX();   tmpy = points[p1].getY();
  points[p1].setX(points[p2].getX());
  points[p1].setY(points[p2].getY());
  points[p2].setX(tmpx);
  points[p2].setY(tmpy);
  //std::cout << points[p1] << "," << points[p2] << std::endl;
}
