# Finding cycles in an undirected graph

This small UndirectedGraph class contains functions to identify cycles in an undirected graph.
It is based on the work described in [this paper](http://dl.acm.org/citation.cfm?doid=363848.363861) _"Algorithms for finding a fundamental set of cycles for an undirected linear graph"_


## Usage
Here is a minimal working example.

```
#include "undirected_graph.h"
#include <vector>
#include <iostream>

//create undirected graph specifying the number of vertices.
//Here we have 5 vertice numbered 0,1,2,3,4.
UndirectedGraph graph(5);
//Add all the edges
graph.addEdge(0,1);
graph.addEdge(1,2);
graph.addEdge(2,3);
graph.addEdge(3,4);
graph.addEdge(4,0);
graph.addEdge(0,3);
//This leads 2 cycles : 0,1,2,3
//                      0,3,4

//Recover the cycles
std::vector< std::vector<int> > cycles = graph.finAllCycles();
//you can print them out
for (unsigned int c = 0; c < cycles.size(); c++) {
  for (unsigned int n = 0; n < cycles[c].size(); n++) {
   std::cout << cycles[c][n] << " ";
  }
  std::cout << "\n";
}
```

Check out the graph.cpp file for some examples.
You can compile it using the _compile_ script provided.

## Algorithms
All the algorithm are described in the previously cited paper.

There is 4 main parts:
1. From the adjency matrix of the graph, create a second graph containing all the disjoint trees corresponding to the initial graph.
2. Find all the connected component of the second graph.
3. Find way to reconnect the connected component in the second graph to achieve a spanning tree.
4. Every edges in the initial graph that is not present in the spanning tree is an edge creating a cycle. Use that to find the cycles and list them.

## Conditions

The graph needs to be connected (all vertices have a path to every other vertices). The algorithm will not detect if it is not connected.

#Graphics

##GraphReader
A rudimentary parser has been implemented to read a graph from a file.
The syntax is the following
```
nb_nodes
nb_edge
e1 e2
e3 e4
.
.
.
```
example :
```
17
20
0 1
0 3
1 2
2 4
2 5
3 4
4 7
5 6
6 8
6 9
7 8
8 13
9 10
10 11
11 12
12 9
13 14
13 16
14 15
15 16
```

Be careful that there is no trailing whitespace in any line! (I told you it is rudimentary...).

##Usage
A executable **img_graph.exe** is create while running
~~~{.sh}
./compile
~~~
This executable takes as input a file name containing a graph description as presented above.

| arg | position | range | default | description |
|-----|----------|-------|---------|-------------|
| file\_name | 1 | valid path | "" | file containing the graph description |
| factor | 2 | 0-1 | 0.01 | factor applied to forces before moving points |
| obj_distance | 3 | 0-inf | 100 | objective size in pixels between the points|
| tol | 4 | 0-inf | -1 | tolerance to consider the position of the graph _converged_. If negative value, an adaptive scheme is used to determined the tolerance. Larger values focuses on swapping edges, smaller focuses on moving the points. |
| size_x | 5 | 0-inf | 500 | x dimension of the shown window |
| size_y | 6 | 0-inf | 400 | y dimension of the shown window |
| verbose | 7 | 0 or 1 | 0 | if 1, prints additional informations. |
| seed | 8 | 0-inf | 0 | Seed that governs all the random number generation (for swapping). if blank or 0, defaults to time(0) |