#include "undirected_graph.h"
#include "graph_reader.h"
#include <string>


int main(int argc, char *argv[])
{
  std::string file_name = "";
  if (argc >= 2) {
    file_name = argv[1];
  }
  GraphReader gr(file_name);
  UndirectedGraph g = gr.read();
  
  std::vector<std::vector<int> > cycles = g.findAllCycles();
  for (unsigned int c = 0; c < cycles.size(); c++) {
    for (unsigned int n = 0; n < cycles[c].size(); n++) {
      std::cout << cycles[c][n] << " ";
    }
    std::cout << "\n";
  }

  g.print();

  return 0;
}
