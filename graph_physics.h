/** 
 * @file graph_physics.h
 *
 * @author Benjamin Paccaud <bpaccaud@gmail.com>
 * 
 * @date 19-Jan-2016
 *
 * @brief 
 */


#ifndef __GRAPH_PHYSICS_H__
#define __GRAPH_PHYSICS_H__

#include "undirected_graph.h"
#include <vector>
#include "point.h"
#include "segment.h"
#include "intersection_helper.h"

///Class to apply forces to points of a graph and try to put it in a planar state.
class GraphPhysics
{
public:
  ///To store a list of points
  /// @todo think about making a class of this so some function can be call on it such as 
  /// norm and move all and such
  typedef std::vector<Point> VecPoint;
  ///constructors
  GraphPhysics(UndirectedGraph _g);
  virtual ~GraphPhysics() {};
  
  //Acessors
public:
  VecPoint& getPoints() { return points; };
  VecPoint& getForces() { return forces; };
  VecPoint& getVectors() { return vectors; };
  std::vector<int>& getConn() { return conn; };
  
  void setGraph(UndirectedGraph new_g);
  void setPoints(std::vector<Point> new_p);
  
  //Methods
  /// take the connectivity \a conn and computes all the vector 
  /// corresponding to the edges.
  void computeVectors();
  /// take the connectivity \a conn and computes all the distances
  /// corresponding to the edges/
  void computeDistances();
  
  ///Take the \a vectors and \a distances and computes the forces to apply
  ///relative to the objective distance \a obj_distance.
  void computeForces(double obj_distance);
  ///Computes electrostatic-like repulsion on each vertices
  void computeRepulsion(double rep_range);
  ///Apply the computed forces to the \a points
  void applyForces(double factor);
  ///Function doing all the work. Zero the forces, recomputes them and apply them to the points.
  void movePoints(double obj, double rep, double factor, bool do_rep = true);

  ///computes the average norm of all the points in \a v. To be applied on vectors (like forces)
  double norm(VecPoint& v);  
  ///checks to average norm of \a forces and compares it to tol
  ///\returns true if the norm is smaller than tol, false otherwise
  bool checkForceNorm(double tol);
  ///Puts all the points in \a v to 0.
  void zerosVecPoint(VecPoint &v);

  ///computes the barycenter of the graph.
  ///\returns the barycenter of the graph.
  Point computeCenter();
  ///computes the center of the bounding box around the graph.
  ///\returns the center of the bounding box around the graph.
  Point computeCenterBox();
  ///apply the translation \a p to all points in \a points
  void translateAll(Point p);

  ///Takes the connectivity and creates a list of segments
  void createSegments();
  ///Takes the list of segments and counts the number of intersections
  ///between segments that do not have node in common
  int countIntersections();

  ///If there is intersections in the graph, try to find a pair of 
  ///segments that could have some points switched to reduce
  ///the global number of interactions
  bool switchPoints(bool strict = true);
  ///Helper function swapping positions of \a p1 and \a p2 and 
  ///recomputes the number of intersection. 
  ///\returns true if the swapping leads to a number of intersections 
  ///smaller than nb_cross. Else it swaps back returns false.
  bool trySwapping(int nb_cross, Point* p1, Point* p2, bool strict = true);
  ///Helper function to define < and <= depending on the boolean \a strict.
  ///\returns val1 < val2 if \a strict is true
  ///\returns val1 <= val2 if \a strict is false
  static bool smaller(double val1, double val2, bool strict);
  ///Function to swap two points in the list of points based on their indexes
  void swap(int p1, int p2);
  
  ///Ratio used to compute the forces apply based on the objective distance (here \a avg)
  static double ratio(double& avg, double& dist) {
    return (dist - avg) / avg;
  };
  ///shifting function for the repulsion
  static double shift(double val, double obj);

  ///gives a random integer number between 0 and max
  int rand(int max);
  
  //Variables
private:
  UndirectedGraph g;
  VecPoint points;
  std::vector<int> conn;
  std::vector< double > distances;
  VecPoint vectors;
  VecPoint forces;
  
  IntersectionHelper inter_helper;

  ///unused
  bool init_rand;

  std::vector<Segment> segments;
};

#endif /* __GRAPH_PHYSICS_H__ */
