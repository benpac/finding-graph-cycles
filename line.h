/** 
 * @file line.h
 *
 * @author Benjamin Paccaud <bpaccaud@gmail.com>
 * 
 * @date 20-Jan-2016
 *
 * @brief 
 */


#ifndef __LINE_H__
#define __LINE_H__
#include "Point.h"
class Line
{
public:
  Line(Point *_p1, Point *_p2) : p1(_p1), p2(_p2) {};
  virtual ~Line() {};
  
  //Acessors
public:
  bool isCrossing(const Line& l);
  
  //Methods
  
  

  //Variables
private:
  Point * p1;
  Point * p2;
  
};

#endif /* __LINE_H__ */
