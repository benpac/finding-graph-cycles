/// Created by Benjamin Paccaud <bpaccaud@gmail.com>
/// 
/// 

#include "undirected_graph.h"
#include <vector>

// Driver program to test above functions
int main()
{
  std::cout << "\nExample 1" << std::endl;
  std::cout << "  0-----1-----2  \n";
  std::cout << "  | \\_/ | \\_/ | \n";
  std::cout << "  | / \\ | / \\ | \n";
  std::cout << "  3-----4-----5 \n";
  std::cout << " Plus 0-5, 2-3 \n";
  std::cout << " Is giving out 8 cycles because they are primary. \n";
  UndirectedGraph g1(6);
  g1.addEdge(0, 1);
  g1.addEdge(0, 3);
  g1.addEdge(0, 4);
  g1.addEdge(0, 5);
  g1.addEdge(1, 3);
  g1.addEdge(1, 4);
  g1.addEdge(1, 5);
  g1.addEdge(1, 2);
  g1.addEdge(2, 3);
  g1.addEdge(2, 4);
  g1.addEdge(2, 5);
  g1.addEdge(3, 4);
  g1.addEdge(4, 5);


  //    g1.print();
  std::vector<std::vector<int> > cycle = g1.findAllCycles();
  for (unsigned int c = 0; c < cycle.size(); c++) {
    for (unsigned int n = 0; n < cycle[c].size(); n++) {
      std::cout << cycle[c][n] << " ";
    }
    std::cout << "\n";
  }


  std::cout << "\nExample 2" << std::endl;
  std::cout << " Paper one  \n";
  std::cout << " 0---2     4  \n";
  std::cout << " | \\ |    /  \n";
  std::cout << " 6---3---1---5  \n";
  UndirectedGraph g2(7);
  g2.addEdge(0, 2);
  g2.addEdge(0, 3);
  g2.addEdge(0, 6);
  g2.addEdge(1, 5);
  g2.addEdge(1, 4);
  g2.addEdge(1, 3);
  g2.addEdge(2, 3);
  g2.addEdge(3, 6);
  // g2.addEdge(4, 7);
  // g2.addEdge(0, 8);
  // g2.addEdge(8, 9);

  //    g2.print();
  cycle = g2.findAllCycles();
  for (unsigned int c = 0; c < cycle.size(); c++) {
    for (unsigned int n = 0; n < cycle[c].size(); n++) {
      std::cout << cycle[c][n] << " ";
    }
    std::cout << "\n";
  }

  std::cout << "\nExample 3" << std::endl;
  std::cout << "      1   5--6----9---10  \n";
  std::cout << "    /  \\ /    \\   |    |  \n";
  std::cout << "   0    2      8  12--11     \n";
  std::cout << "    \\   |    /  \\  \n";
  std::cout << "     3--4---7    13---14  \n";
  std::cout << "                 |    |  \n";
  std::cout << "                 16---15  \n";
  UndirectedGraph g3(17);
  g3.addEdge(0, 1);
  g3.addEdge(0, 3);
  g3.addEdge(1, 2);
  g3.addEdge(2, 4);
  g3.addEdge(2, 5);
  g3.addEdge(3, 4);
  g3.addEdge(4, 7);
  g3.addEdge(5, 6);
  g3.addEdge(6, 8);
  g3.addEdge(6, 9);
  g3.addEdge(7, 8);
  g3.addEdge(8, 13);
  g3.addEdge(9, 10);
  g3.addEdge(10, 11);
  g3.addEdge(11, 12);
  g3.addEdge(12, 9);
  g3.addEdge(13, 14);
  g3.addEdge(13, 16);
  g3.addEdge(14, 15);
  g3.addEdge(15, 16);

  cycle = g3.findAllCycles();
  for (unsigned int c = 0; c < cycle.size(); c++) {
    for (unsigned int n = 0; n < cycle[c].size(); n++) {
      std::cout << cycle[c][n] << " ";
    }
    std::cout << "\n";
  }

  std::cout << "\nExample 4" << std::endl;
  std::cout << "      1   3--4----9---10  \n";
  std::cout << "    /  \\ /    \\   |    |  \n";
  std::cout << "   0    2      5  12--11     \n";
  std::cout << "    \\   |    /  \\  \n";
  std::cout << "     8--7---6    13---14  \n";
  std::cout << "                 |    |  \n";
  std::cout << "                 16---15  \n";
    
  UndirectedGraph g4(17);
  g4.addEdge(0, 1);
  g4.addEdge(1, 2);
  g4.addEdge(2, 3);
  g4.addEdge(3, 4);
  g4.addEdge(4, 5);
  g4.addEdge(5, 6);
  g4.addEdge(6, 7);
  g4.addEdge(7, 8);
  g4.addEdge(2, 7);
  g4.addEdge(0, 8);
  g4.addEdge(4, 9);
  g4.addEdge(9, 10);
  g4.addEdge(10, 11);
  g4.addEdge(11, 12);
  g4.addEdge(12, 9);
  g4.addEdge(5, 13);
  g4.addEdge(13, 14);
  g4.addEdge(13, 16);
  g4.addEdge(14, 15);
  g4.addEdge(15, 16);

  // g4.addEdge(9, 13);
  g4.addEdge(11, 14);



  cycle = g4.findAllCycles();
  for (unsigned int c = 0; c < cycle.size(); c++) {
    for (unsigned int n = 0; n < cycle[c].size(); n++) {
      std::cout << cycle[c][n] << " ";
    }
    std::cout << "\n";
  }

  std::cout << "\nExample 5" << std::endl;
  std::cout << "    0---1---2  \n";
  std::cout << "   / \\ / \\ / \\  \n";
  std::cout << "  3---4---5---6  \n";
  std::cout << "   \\ / \\ / \\ /   \n";
  std::cout << "    7---8---9  \n";
  UndirectedGraph delaunay(10);
  delaunay.addEdge(0, 1);
  delaunay.addEdge(0, 3);
  delaunay.addEdge(0, 4);
  delaunay.addEdge(1, 2);
  delaunay.addEdge(1, 5);
  delaunay.addEdge(1, 4);
  delaunay.addEdge(2, 5);
  delaunay.addEdge(2, 6);
  delaunay.addEdge(3, 4);
  delaunay.addEdge(3, 7);
  delaunay.addEdge(4, 5);
  delaunay.addEdge(4, 7);
  delaunay.addEdge(4, 8);
  delaunay.addEdge(5, 6);
  delaunay.addEdge(5, 8);
  delaunay.addEdge(5, 9);
  delaunay.addEdge(6, 9);
  delaunay.addEdge(7, 8);
  delaunay.addEdge(8, 9);

  //std::cout << "\n\n\n" << std::endl;
  //    g2.print();

  cycle = delaunay.findAllCycles();
  for (unsigned int c = 0; c < cycle.size(); c++) {
    for (unsigned int n = 0; n < cycle[c].size(); n++) {
      std::cout << cycle[c][n] << " ";
    }
    std::cout << "\n";
  }


  std::cout << "\nExample 6" << std::endl;
  std::cout << "  0--1--2--3  \n";
  std::cout << "  |  |  |  |  \n";
  std::cout << "  4--5--6--7  \n";
  std::cout << "  |  |  |  |  \n";
  std::cout << "  8--9--10-11  \n";
  UndirectedGraph squares(12);
  squares.addEdge(0, 1);
  squares.addEdge(0, 4);
  squares.addEdge(1, 2);
  squares.addEdge(1, 5);
  squares.addEdge(2, 3);
  squares.addEdge(2, 6);
  squares.addEdge(3, 7);
  squares.addEdge(4, 5);
  squares.addEdge(4, 8);
  squares.addEdge(5, 6);
  squares.addEdge(5, 9);
  squares.addEdge(6, 7);
  squares.addEdge(6, 10);
  squares.addEdge(7, 11);
  squares.addEdge(8, 9);
  squares.addEdge(9, 10);
  squares.addEdge(10, 11);


  //std::cout << "\n\n\n" << std::endl;
  //    g2.print();

  cycle = squares.findAllCycles();
  for (unsigned int c = 0; c < cycle.size(); c++) {
    for (unsigned int n = 0; n < cycle[c].size(); n++) {
      std::cout << cycle[c][n] << " ";
    }
    std::cout << "\n";
  }


  std::cout << "\nExample 7" << std::endl;
  std::cout << "          0--1  \n";
  std::cout << "         /    \\   \n";
  std::cout << "     2--3      4--5  \n";
  std::cout << "    /    \\    /    \\  \n";
  std::cout << "   6      7--8      9  \n";
  std::cout << "    \\    /    \\    /  \n";
  std::cout << "    10--11    12--13  \n";
  std::cout << "         \\    /   \n";
  std::cout << "         14--15  \n";
  UndirectedGraph hexagones(16);
  hexagones.addEdge(0, 1);
  hexagones.addEdge(0, 3);
  hexagones.addEdge(1, 4);
  hexagones.addEdge(2, 3);
  hexagones.addEdge(2, 6);
  hexagones.addEdge(3, 7);
  hexagones.addEdge(4, 8);
  hexagones.addEdge(4, 5);
  hexagones.addEdge(5, 9);
  hexagones.addEdge(6, 10);
  hexagones.addEdge(7, 8);
  hexagones.addEdge(7, 11);
  hexagones.addEdge(8, 12);
  hexagones.addEdge(9, 13);
  hexagones.addEdge(10 ,11);
  hexagones.addEdge(11, 14);
  hexagones.addEdge(12, 13);
  hexagones.addEdge(12, 15);
  hexagones.addEdge(14, 15);

  // hexagones.addEdge(3, 4);
  // hexagones.addEdge(6, 7);
  // hexagones.addEdge(8, 9);
  // hexagones.addEdge(11, 12);

  //std::cout << "\n\n\n" << std::endl;
  //    g2.print();

  cycle = hexagones.findAllCycles();
  for (unsigned int c = 0; c < cycle.size(); c++) {
    for (unsigned int n = 0; n < cycle[c].size(); n++) {
      std::cout << cycle[c][n] << " ";
    }
    std::cout << "\n";
  }


  return 0;
}
