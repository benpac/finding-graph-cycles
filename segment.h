/** 
 * @file segment.h
 *
 * @author Benjamin Paccaud <bpaccaud@gmail.com>
 * 
 * @date 27-Jan-2016
 *
 * @brief 
 */


#ifndef __SEGMENT_H__
#define __SEGMENT_H__

#include "point.h"

/// see <a "http://www.dcs.gla.ac.uk/~pat/52233/slides/Geometry1x1.pdf">this presentation</a>
/// or <a "http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/">this page</a>
class Segment
{ 
public:
  Segment(Point *_p0, Point *_p1);
  virtual ~Segment() {};
  
  //Acessors
public:
  Point *getP0() const { return p0; };
  Point *getP1() const { return p1; };

  Point getVector() { return p0->getVector(*p1); };
  
  //Methods

  //Variables
private:
  Point *p0;
  Point *p1;
};

#endif /* __SEGMENT_H__ */
