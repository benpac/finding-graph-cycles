
#include "point.h"
#include "segment.h"
#include "intersection_helper.h"
#include <iostream>

int main(int argc, char *argv[])
{
  Point p1(0,0);
  Point p2(15,15);
  Point q1(0,15);
  Point q2(15,0);

  Segment s1(&p1,&p2);
  Segment s2(&q1,&q2);
  
  IntersectionHelper IH;

  std::cout << "are they intersecting  ? " << (IH.intersects(s1,s2) ? " True " : " False ") << std::endl;
  
  //q2.setX(0);
  p1.swap(q1);
  std::cout << "are they intersecting  ? " << (IH.intersects(s1,s2) ? " True " : " False ") << std::endl;
  q1.swap(p1);
  
  q2.setY(5);
  std::cout << "are they intersecting  ? " << (IH.intersects(s1,s2) ? " True " : " False ") << std::endl;

  q2.setX(2);
  std::cout << "are they intersecting  ? " << (IH.intersects(s1,s2) ? " True " : " False ") << std::endl;
  

  return 0;
}
