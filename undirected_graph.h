/** 
 * @file undirected_graph.h
 *
 * @author Benjamin Paccaud <bpaccaud@gmail.com>
 * 
 * @date 09-Dec-2015
 *
 * @brief 
 */


#ifndef __UNDIRECTED_GRAPH_H__
#define __UNDIRECTED_GRAPH_H__

#include <iostream>
#include <list>
#include <limits.h>
#include <vector>
/// Based on the paper "Algorithms for finding a fundamental set of cycles for an undirected
/// linear graph" http://dl.acm.org/citation.cfm?doid=363848.363861. 
/// C. C. Gotlieb and D. G. Corneil
class UndirectedGraph
{
private:
  ///Private Matrix class to store adjency matrix.
  class Matrix {
  public:
    ///Constructor. requires number of rows and columns
    Matrix(int r, int c) :
      cols(c),
      rows(r),
      m(cols*rows,0)
    {};

    Matrix(const Matrix& other_m) :
      cols(other_m.getNbCols()),
      rows(other_m.getNbRows()),
      m(other_m.m) {};
    
    virtual ~Matrix() {};
    ///This function grows or reduce the size of the matrix by changinf the number of 
    ///rows
    void changeNbRows(int nb_rows) {
      //std::vector<int> tmp = m;
      m.resize((nb_rows)*cols,0);
      rows = nb_rows;
      
    };

    ///Getter of the number of rows
    int getNbRows() const {return rows;};
    ///Getter of the number of columns
    int getNbCols() const {return cols;};
    ///Element accessor.
    int& at(int i, int j) {
      return m[i*cols + j];
    }
    
    ///element accessor directly to vector
    int& operator[](int index) {
      return m[index];
    }

    ///Const accessor
    int at(int i, int j) const {
      return m[i*cols + j];
    }
    ///Prints out the matrix rows by rows
    void print() const {
      for ( int i = 0; i < rows; i++) {
        for ( int j = 0; j < cols; j++) {
          std::cout << this->at(i,j) << " ";
        }
        std::cout << "\n";
      }
    }
  private:
    //Variables
    ///Number of columns
    int cols;
    ///Number of rows
    int rows;
    ///Internal storage of the matrix
    std::vector< int > m;
  };

  ///Private Matrix class for square matrices that will represent the 
  ///Adjency matrix
  class AdjMatrix : public Matrix {
  public:
    ///Constructor taking size of square matrix
    AdjMatrix(int n) :
      Matrix(n,n)
    {};
    
    AdjMatrix(const AdjMatrix& other_a) :
      Matrix(other_a)
    {};
    
    virtual ~AdjMatrix() {};
  private:
    //Variables
  };

public:
  ///Constructor. Take number of vertices to create the adjency matrix of right size
  UndirectedGraph(int _V);
  UndirectedGraph(const UndirectedGraph& g);
  virtual ~UndirectedGraph() {};
  
  //Acessors
public:
  ///get nb of nodes
  unsigned int getV() const { return V; };
  unsigned int getE() const { return E; };
  
  AdjMatrix getAdj() const {return adj;};
  AdjMatrix getB() const {return B;};
  Matrix getC() const {return C;};

  
  std::vector<int> getEdges() ; 
  std::vector<int> getEdgesSpanningTree() ; 
private :
  std::vector<int> getEdges(AdjMatrix& m) ; 
  
public:
  //Methods
  void addEdge(int v, int w);   ///< to add an edge to the UndirectedGraph
  ///Public function to compute and return all the cycles
  std::vector< std::vector<int> > findAllCycles();

private:
  ///Partition the graph into disjoint trees (creates B)
  void partitionInDisjointTree(); //block 1
  ///Find and extract the connected component from the modified Adjency matrix B.
  ///(Fills the matrix C)
  void findConnectedComponent(); //block 2
  //goes trough the matrix C and find a minimal way to connect the components into a 
  ///spanning tree.
  void joinConnectedComponent(); //block 3
  ///Finds all the cycles in the graph from the modified B matrix
  void findCycles(); //block 4
public:
  ///Prints the internal variables (V, adj, B and C)
  void print() {
    std::cout << "V : " << V << std::endl;
    std::cout << "adj\n";
    adj.print();
    std::cout << "B\n";
    B.print();
    std::cout << "C\n";
    C.print();
  };


private:
  int V;    ///< Nb. of vertices
  int E; ///< Nb. of edges
  AdjMatrix adj; ///< Adjency matrix of graph.
  AdjMatrix B; ///< Adjency matrix of spanning tree of graph.
  Matrix C; ///< Component decomposition of graph.
  
  ///Stores all the minimal cycles found.
  std::vector< std::vector<int> > min_cycles;


};

#endif /* __UNDIRECTED_GRAPH_H__ */

