/** 
 * @file point.h
 *
 * @author Benjamin Paccaud <bpaccaud@gmail.com>
 * 
 * @date 15-Jan-2016
 *
 * @brief 
 */


#ifndef __POINT_H__
#define __POINT_H__

#include <iostream>
#include <cmath>


class Point
{
public:
  Point() : x(0.), y(0.) {};
  Point(const Point& p) : x(p.x), y(p.y) {};
  Point(double _x, double _y) : x(_x), y(_y) {};
  virtual ~Point() {};
  
  //Acessors
public:
  double getX() const { return x; };
  double getY() const { return y; };
  
  void setX(double _x) { x = _x; };
  void setY(double _y) { y = _y; };
  
  void zero() { x = 0.; y = 0.; };
  //Methods
  double distance2(const Point& p) {
    return (p.getX() - x) * (p.getX() - x) + (p.getY() - y) * (p.getY() - y);
  };

  double distance(const Point& p) {
    return std::sqrt(this->distance2(p));
  };

  Point getVector(const Point& p) {
    return Point(p.getX() - x, p.getY() - y);
  };

  friend Point operator+(Point base, const Point& p) {
    base += p;
    return base;
  };

  Point& operator+=(const Point& p) {
    x += p.getX();
    y += p.getY();
    return *this;
  };

  Point operator-() {
    return Point(-x,-y);
  };

  Point operator*(double fac) {
    return Point(x*fac,y*fac);
  };

  Point operator/(double fac) {
    return Point(x/fac,y/fac);
  };

  double cross(const Point& p) {
    return x * p.getY() - y * p.getX();
  };

  void unify() {
    double d = this->norm();//std::sqrt(x*x + y*y);
    x /= d;
    y /= d;
  };

  double norm() {
    return std::sqrt(x*x + y*y);
  };

  void swap(Point& p) {
    double tmpx = x;
    double tmpy = y;
    x = p.getX(); y = p.getY();
    p.setX(tmpx); p.setY(tmpy);
  }


  friend std::ostream& operator<<(std::ostream& os, const Point& p) {
    os << "[" << p.x << ", " << p.y << "]";
    return os;
  };
  

  //Variables
private:
  double x;
  double y;
};

#endif /* __POINT_H__ */
