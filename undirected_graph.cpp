#include "undirected_graph.h"
#include <deque>
#include <algorithm>

UndirectedGraph::UndirectedGraph(int _V) :
  V(_V),
  E(0),
  adj(V),
  B(V),
  C(0,V)
{}


UndirectedGraph::UndirectedGraph(const UndirectedGraph& g) :
  V(g.getV()),
  E(g.getE()),
  adj(g.getAdj()),
  B(g.getB()),
  C(g.getC())
{}

void UndirectedGraph::addEdge(int v, int w)
{
  adj.at(v, w) = 1; // Add w to v’s list.
  adj.at(w, v) = 1; // Add v to w’s list.
  E += 1;
}

void UndirectedGraph::partitionInDisjointTree()
{
  for ( int line = 0; line < V; line++) {
    for ( int col = line+1; col < V; col++) {
      if (adj.at(line,col) == 1) {
        B.at(line,col ) = 1;
        B.at(col ,line) = 1;
        break;
      }
    }
  }
}

void UndirectedGraph::findConnectedComponent()
{
  //vector storing which nodes are already considered connected
  std::vector<int> x(V, 0);
  int nb_conn_comp(0);
  for ( int n = 0; n < V; ++n) {
    if (x[n] == 1) {
      continue;
    }
    nb_conn_comp++;
    C.changeNbRows(nb_conn_comp);
    std::deque<int> to_visit;
    to_visit.push_back(n);
    //while there is nodes connected, read line of B and add all connected nodes to list of 
    //nodes to look at.
    while( !to_visit.empty() ) {
      int visiting = to_visit.back();
      to_visit.pop_back();
      if (!(x[visiting] == 1) ) { 
        for ( int nB = 0; nB < V; ++nB) {
          if (B.at(visiting, nB) == 1) {
            if (x[visiting] == 0) {
              to_visit.push_back(nB);
            }
          }
        }
        x[visiting] = 1;
        C.at(nb_conn_comp-1, visiting) = 1;
      }
    } 
  }
}

void UndirectedGraph::joinConnectedComponent()
{
  std::vector<int> size_conn_comp(C.getNbRows(),0);
  for ( int row = 0; row < C.getNbRows(); row++) {
    for ( int col = 0; col < V; col++) {
      size_conn_comp[row] += C.at(row,col);
    }
  }
  for (unsigned int r = 0; r < size_conn_comp.size(); r++) {
    std::vector<int>::iterator min = std::min_element(size_conn_comp.begin(),size_conn_comp.end());
    int rr(min - size_conn_comp.begin());
    int node(0);
    bool worked = false;
    // for the number of 1 in the row \a rr
    for ( int m = 0; m < *min; ++m) {
      if (worked) { break;}
      int sel(0);
      //find the sel-th node with a 1 in row \a rr
      for ( int c = 0; c < V; ++c) {
        if (C.at(rr,c) == 1) {
          if (sel == m) {
            node = c; break;
          }
          sel++;
        }
      }
      bool worked_inside = false;
      //for each component that is not the one considered
      for ( int Cr = 0; Cr < C.getNbRows(); Cr++) {
        if (Cr == rr) {continue;}
        //look at each connected node to \a node in \a adj
        for ( int Bn = 0; Bn < V; Bn++) {
          if (adj.at(node,Bn) == 1) {
            //check if in the component at row Cr
            if (C.at(Cr,Bn) == 1) {
              //update C
              for ( int c = 0; c < V; c++) {
                if (C.at(rr,c) == 1) {
                  C.at(Cr,c) = 1;
                  C.at(rr,c) = 0;
                }
              }
              size_conn_comp[Cr] += *min; //add all the 1s in row rr to row Cr.
              //update B
              B.at(node,Bn) = 1;
              B.at(Bn,node) = 1;
              worked = true;
              worked_inside = true;
              break;
            }
          }
        }
        if (worked_inside) { break;}
      }
    }
    *min +=V;
  }

}

void UndirectedGraph::findCycles()
{
  // Find edges that are creating cycles.
  // For each, start from one end and search in B for the other end.
  // Save node visited during search until found other end
  // Cycle will be the list of visited nodes.
  
  std::deque< std::pair<int,int> > cycle_edges;
  for ( int i = 0; i < V; i++) {
    for ( int j = i+1; j < V; j++) {
      if (B.at(i,j) != adj.at(i,j)) {
        cycle_edges.push_back(std::pair<int,int>(i,j));
      }
    }
  }

  int start_node;
  int end_node;
  for (unsigned int e = 0; e < cycle_edges.size(); e++) {
    start_node = cycle_edges[e].first;
    end_node = cycle_edges[e].second;
     std::vector<int> cycle;
    std::deque<int> to_visit;
    //to not revisit nodes already visited
    std::vector<bool> visited(V,false);
    visited[start_node] = true;
    to_visit.push_back(start_node);
    bool success = false;
    while (!success && !to_visit.empty()) {
       //get node to check and add it to cycle
      int node = to_visit.back();
      to_visit.pop_back();
      visited[node] = true;
      cycle.push_back(node);
      //loop on line in B and find all connected (in spanning tree) and not already visited
      for ( int b = 0; b < V; b++) {
        if(B.at(node,b) == 1 && !visited[b]) {
          if (b == end_node) { //if it is the end node, we can stop
            cycle.push_back(b); 
            success = true;
            break;
          }
          //we add to list to visit
          to_visit.push_back(b);
        }
      }
    }
    if (success) {
       //We now have a cycle but ther might be branches or it might be a longer loop than the minimal we want.
      int cycle_size = cycle.size();
      //Goes through the nodes in cycle and checks if one of the nodes later in the vector is connected to 
      // a node before. This indicates that the nodes in between are not part of the cycle.
      int cycle_size2;
      for ( int c = 0; c < cycle_size-1; c++) {
        cycle_size2 = c == 0 ? cycle_size-1 : cycle_size;
        for ( int cc = c+1; cc < cycle_size2; cc++) {
          if (cc > c+1) {
             //we erase only if there exist an edge between cycle[c] and cycle[cc] 
            //and cycle[cc] is connected to cycle[cc+1] (if cc < cycle_size-1)
            if (adj.at(cycle[c], cycle[cc]) == 1 &&
                (cc == cycle_size-1 ||
                 adj.at(cycle[cc], cycle[cc+1]) == 1) )
              {
              for ( int er = c+1; er < cc; er++) {
                
                cycle.erase(cycle.begin()+(c+1));
                cycle_size--;
                cycle_size2--;
              }
              //we erased stuff so we start again at cc = c+1
              cc = c+1;
            }
          }
        }
      }
      min_cycles.push_back(cycle);
    } else //not success
      { std::cout << "GRAPH FIND CYCLE NOT SUCCESS!" << std::endl; }
  }
}


std::vector< std::vector<int> >  UndirectedGraph::findAllCycles() {
  partitionInDisjointTree();
  findConnectedComponent(); 
  joinConnectedComponent(); 
  findCycles();
  return min_cycles;
}

std::vector<int> UndirectedGraph::getEdges()  {
  return getEdges(adj);
}
std::vector<int> UndirectedGraph::getEdgesSpanningTree()  {
  return getEdges(B);
}
std::vector<int> UndirectedGraph::getEdges(AdjMatrix& m)  {
  std::vector<int> ret;
  for ( int r = 0; r < m.getNbRows(); r++) {
    for ( int c = r+1; c < m.getNbCols(); c++) {
      if (m.at(r,c) == 1) {
        ret.push_back(r);
        ret.push_back(c);
      }
    }
  }
  if ((int)(ret.size() / 2) != E) {
    std::cout << "There might be a miscountage of edges... size of vec :" << ret.size() << std::endl;
  }
  return ret;
}
