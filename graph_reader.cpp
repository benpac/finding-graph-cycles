#include "graph_reader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

UndirectedGraph GraphReader::read() {
  std::ifstream graph_file;
  //std::cout << "file_name : " << file_name << std::endl;
  graph_file.open(file_name.c_str());
  if (graph_file.is_open()) {
    std::vector<std::string> lines;
    std::string line;
    while (!graph_file.fail() && (!graph_file.eof())) {
      //std::cout << "reading line" << std::endl;
      getline(graph_file, line);
      lines.push_back(line);
    }
    //std::cout << "nb lines in file : " << lines.size() << std::endl;
    int V = getInt(lines[0]);
    UndirectedGraph g(V);
    int E = getInt(lines[1]);
    //    std::cout << "V and E : "<< V << " "<< E << std::endl;
    int start = 2;
    for ( int e = 0; e < E; e++) {
      if ( (start + e) >= (int)(lines.size()) ) {break;}
      std::vector<std::string> conn = split(lines[start + e], ' ');
      if (conn.size() == 2) {
        int v = getInt(conn[0]);
        int w = getInt(conn[1]);
        if (v >= 0 && v < V && w >= 0 && w < V) {
          g.addEdge(v,w);
          //          std::cout << "adding : " << v << "," << w << std::endl;
        } else {
          std::cout << "GraphReader : Bad edge definition : " << v <<" "<< w << std::endl;
        }
      }
    }
    graph_file.close();
    return g;
  }
  else {
    std::cout << "Error while reading file. Returning empty graph" << std::endl;
    return UndirectedGraph(0);
  }
}

UndirectedGraph GraphReader::read(std::string file) {
  file_name = file;
  return read();
}

std::vector<std::string> &GraphReader::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> GraphReader::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

int GraphReader::getInt(std::string str) {
  std::stringstream ss(str);
  //  std::cout << "getInt str is : " << str << std::endl;
  int i;
  ss >> i;
  if (!ss) { std::cout << "Error parsing integer" << std::endl; 
  i =0;
  }
  return i;
}

double GraphReader::getDouble(std::string str) {
  std::stringstream ss(str);
  int d;
  ss >> d;
  if (!ss) { std::cout << "Error parsing double" << std::endl; 
  d =0.;
  }
  return d;
}
